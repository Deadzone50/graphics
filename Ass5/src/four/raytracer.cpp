#include "raytracer.hpp"

#include "args.hpp"
#include "Camera.h"
#include "hit.hpp"
#include "lights.hpp"
#include "material.hpp"
#include "objects.hpp"
#include "ray.hpp"
#include "SceneParser.h"

#define EPSILON 0.001f

using namespace std;
using namespace FW;

namespace {

// Compute the mirror direction for the incoming direction, given the surface normal.
Vec3f mirrorDirection(const Vec3f& normal, const Vec3f& incoming)
{
	// YOUR CODE HERE (R8)
	// Pay attention to the direction which things point towards, and that you only
	// pass in normalized vectors.
	Vec3f M = incoming - 2*(incoming.dot(normal))*normal;
	return M;
}

bool transmittedDirection(const Vec3f& normal, const Vec3f& incoming, 
		float index_i, float index_t, Vec3f& transmitted) {
	// YOUR CODE HERE (EXTRA)
	// Compute the transmitted direction for the incoming direction, given the surface normal
	// and indices of refraction. Pay attention to the direction which things point towards!
	// You should only pass in normalized vectors!
	// The function should return true if the computation was successful, and false
	// if the transmitted direction can't be computed due to total internal reflection.
	return true;
}

} // namespace

Vec3f RayTracer::traceRay(Ray& ray, float tmin, int bounces, float refr_index, Hit& hit) const {
	// initialize a hit to infinitely far away
	hit = Hit(FLT_MAX);

	// Ask the root node (the single "Group" in the scene) for an intersection.
	bool intersect = scene_.getGroup()->intersect(ray, hit, tmin);

	// Write out the ray segment if visualizing (for debugging purposes)
	if (debug_trace)
		debug_rays.push_back(RaySegment(ray.origin, ray.direction.normalized()*min(100.0f,hit.t), hit.normal));

	// if the ray missed, we return the background color.
	if (!intersect)
		return scene_.getBackgroundColor();
	
	Material* m = hit.material;
	assert(m != nullptr);

	// get the intersection point and normal.
	Vec3f normal = hit.normal;
	Vec3f point = ray.pointAtParameter(hit.t);

	// YOUR CODE HERE (R1)
	// Apply ambient lighting using the ambient light of the scene
	// and the diffuse color of the material.


	
	Vec3f answer = m->diffuse_color(point)*scene_.getAmbientLight();	//ambient light

	// YOUR CODE HERE (R4 & R7)
	// For R4, loop over all the lights in the scene and add their contributions to the answer.

	// If you wish to include the shadow rays in the debug visualization, insert the segments
	// into the debug_rays list similar to what is done to the primary rays after intersection.

	// For R7, if args_.shadows is on, also shoot a shadow ray from the hit point to the light
	// to confirm it isn't blocked; if it is, ignore the contribution of the light.
	int Lights = scene_.getNumLights();
	
	for(int i = 0;i < Lights; ++i)
	{
		Light* L = scene_.getLight(i);
		Vec3f dir_to_light, incident_intensity;
		float distance;
		L->getIncidentIllumination(point, dir_to_light, incident_intensity, distance);
		if(args_.shadows)
		{
			/*
			Ray ShadowRay(point, dir_to_light);		//points towards light
			Hit ShadowHit(distance);
			bool Shadow = scene_.getGroup()->intersect(ShadowRay, ShadowHit, 0.01);
			if(debug_trace)
				debug_rays.push_back(RaySegment(ShadowRay.origin, ShadowRay.direction.normalized()*min(100.0f, ShadowHit.t), ShadowHit.normal));
			if(Shadow)
			{
				continue;								//if it is in the shadow skip the addition
			}*/
			float t = 0;
			float e = 0.01;								//epsilon
			float Attunation = 1;
			Vec3f P = point;
			bool Run = true;
			Hit ShadowHit;
			Ray ShadowRay(P, dir_to_light);
			bool Collision;
			do
			{
				ShadowHit = Hit(distance);		
				ShadowRay = Ray(P, dir_to_light);			//points towards light
				Collision = scene_.getGroup()->intersect(ShadowRay, ShadowHit, e);	//check for collisions
				if(debug_trace)
					debug_rays.push_back(RaySegment(ShadowRay.origin, ShadowRay.direction.normalized()*min(100.0f, ShadowHit.t), ShadowHit.normal));
				if(Collision)
				{
					if(ShadowHit.material->transparent_color(0).length() > 0.0f)		//if the object is transparent
					{
						float t0 = ShadowHit.t;					//first collision with transparent object
						P = P + (t0)*dir_to_light;		//collision point
						ShadowHit = Hit(distance);
						ShadowRay = Ray(P, dir_to_light);			//points towards light
						Collision = scene_.getGroup()->intersect(ShadowRay, ShadowHit, e);	//check for collisions
						if(debug_trace)
							debug_rays.push_back(RaySegment(ShadowRay.origin, ShadowRay.direction.normalized()*min(100.0f, ShadowHit.t), ShadowHit.normal));
						float t1 = ShadowHit.t;					//second collision, doesnt work if light is inside object
						Attunation *= 1/t1;
						
						t = t1;
						P = P + (t)*dir_to_light;			//collision point, on the other side of object now
					}
					else
					{
						Run = false;			//object is not transparent
						continue;
					}
				}
				else
				{
					Run = false;			//didn't hit anything
					answer += m->shade(ray, hit, dir_to_light, incident_intensity*Attunation, 0);	//no shadows on this point
					continue;
				}

			}
			while(Run);
		}
		else
		{
			answer += m->shade(ray, hit, dir_to_light, incident_intensity, 0);	//no shadows
		}
	}
	// are there bounces left?
	if (bounces >= 1)
	{
		--bounces;
		// reflection, but only if reflective coefficient > 0!
		if (m->reflective_color(point).length() > 0.0f)
		{
			// YOUR CODE HERE (R8)
			// Generate and trace a reflected ray to the ideal mirror direction and add
			// the contribution to the result. Remember to modulate the returned light
			// by the reflective color of the material of the hit point.
			Ray MirrorRay(point, mirrorDirection(normal, ray.direction));
			Hit MirrorHit(FLT_MAX);
			Vec3f MirrorColor = traceRay(MirrorRay, 0.01, bounces, refr_index, MirrorHit);

			//float F0 = 0.05;
			//float Fresnel = F0 + (1-F0)*pow((1-(normal.dot(-ray.direction))),5);


			answer += MirrorColor*m->reflective_color(point);
		}

		// refraction, but only if surface is transparent!
		if (m->transparent_color(point).length() > 0.0f)
		{
			// YOUR CODE HERE (EXTRA)
			// Generate a refracted direction and trace the ray. For this, you need
			// the index of refraction of the object. You should consider a ray going through
			// the object "against the normal" to be entering the material, and a ray going
			// through the other direction as exiting the material to vacuum (refractive index=1).
			// (Assume rays always start in vacuum, and don't worry about multiple intersecting
			// refractive objects!) Remembering this will help you figure out which way you
			// should use the material's refractive index. Remember to modulate the result
			// with the material's refractiveColor().
			// REMEMBER you need to account for the possibility of total internal reflection as well.

			Vec3f Normal = hit.normal;
			Vec3f DirIn = ray.direction;
			
			Vec3f I;
			Vec3f N;
			Vec3f T;
			float n1;
			float n2;
			bool TotalRef = false;
			if(DirIn.dot(Normal) > 0)				//"inside" a material
			{
				n1 = m->refraction_index(point);
				n2 = 1;
				N = -Normal;
			}
			else
			{
				n1 = 1;
				n2 = m->refraction_index(point);
				N = Normal;
			}
			float n = n1/n2;

			I = -DirIn;
			
			
			float Sr = 1 - (n*n) * (1- ((N.dot(I))*(N.dot(I))) );
			if(Sr < 0)
			{
				T = mirrorDirection(Normal, DirIn);
				TotalRef = true;
				//return answer;
			}
			else
			{
				float SR = sqrtf(Sr);
				T = (n * (N.dot(I)) - SR) * N - n*I;
				
			}
			Vec3f DirOut = T.normalized();
			/*												//tried to implement using only my physics knowledge... didn't work
			float AngleIn;
			float CosIn =(-DirIn).dot(Normal);
			
			if(CosIn < 0)				//"inside" a material
			{
				CosIn = -CosIn;
				n1 = m->refraction_index(point);
				n2 = 1;

				float TotalRefAngle = asinf(n2/n1);

				AngleIn = acosf(CosIn);

				if(AngleIn >= TotalRefAngle)		//total reflection
				{
					Ray MirrorRay(point, mirrorDirection(Normal, DirIn));
					Hit MirrorHit(FLT_MAX);
					Vec3f MirrorColor = traceRay(MirrorRay, 0.01, bounces, m->refraction_index(point), MirrorHit);
					answer += MirrorColor*m->reflective_color(point);
					return answer;
				}
			}
			else
			{						//"outside"
				n1 = 1;
				n2 = m->refraction_index(point);
				AngleIn = acosf(CosIn);
			}
			
			float SinOut = sinf(AngleIn) * n1/n2;		//snell's law
			float AngleOut = asinf(SinOut);				//angle from the normal of the refracted ray

			float SinIn = sinf(AngleIn);
			float CosOut = cosf(AngleOut);

			Vec3f M = (DirIn + CosIn*Normal)/SinIn;
			Vec3f DirOut = (SinOut*M - CosOut*Normal).normalized();		//wrong angle for material->outside
			float Diff = 180.0f/3.14 * acosf(DirOut.dot(Normal));
			*/
			Hit RefractionHit(FLT_MAX);
			Ray RefractionRay(point, DirOut);
			Vec3f RefractionColor = traceRay(RefractionRay, 0.01, bounces, n2, RefractionHit);


			//float F0 = (n1 - n2)/(n1+n2);
			//float Fresnel = F0 + (1-F0)*pow((1-(normal.dot(-ray.direction))), 5);

			if(!TotalRef)
				answer += RefractionColor* m->transparent_color(point);		//there is no refrative color variable
			else
				answer += RefractionColor;		//total reflection
		}
	}
	return answer;
}
