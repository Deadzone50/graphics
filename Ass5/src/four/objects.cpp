#include "objects.hpp"

#include "hit.hpp"
#include "VecUtils.h"

#include <cassert>

using namespace std;
using namespace FW;

Object3D* Group::operator[](int i) const {
	assert(i >= 0 && i < size());
	return objects_[i].get();
}

void Group::insert(Object3D* o) {
	assert(o);
	objects_.emplace_back(o);
}

bool Group::intersect(const Ray& r, Hit& h, float tmin) const {
	// We intersect the ray with each object contained in the group.
	bool intersected = false;
	for (int i = 0; i < size(); ++i) {
		Object3D* o = objects_[i].get();
		assert(o != nullptr);
		assert(h.t >= tmin);
		bool tmp = o->intersect(r, h, tmin);
		assert(h.t >= tmin);
		if (tmp)
			intersected = true;
	}
	assert(h.t >= tmin);
	return intersected;
}

bool Box::intersect(const Ray& r, Hit& h, float tmin) const
{
	// YOUR CODE HERE (EXTRA)
	// Intersect the box with the ray!
	Vec3f NormX(1,0,0), NormY(0,1,0), NormZ(0,0,1);		//normals along the axes
	Vec3f Norm[6];

	Vec3f Min = min_;
	Vec3f Max = max_;
	Max.z = -Max.z;						//convert back the changes made in Box() to real values
	Max.z += 2*min_.z;					//the preview thing looks wrong othervise

	Norm[0] = -NormX;
	Norm[1] = +NormX;
	Norm[2] = +NormY;
	Norm[3] = -NormY;
	Norm[4] = -NormZ;
	Norm[5] = +NormZ;

	Vec3f Point[6];
	Point[0] = Min;
	Point[1] = Max;
	Point[2] = Max;
	Point[3] = Min;
	Point[4] = Min;
	Point[5] = Max;

	float t;							//current hit
	float t0 = h.t;						//closest hit
	int Side = -1;						//which side was hit
			
	for(int i = 0; i < 6; ++i)			//stole the plane implementation
	{
		float D = -Norm[i].dot(Point[i]);
		t  = (-D -Norm[i].dot(r.origin))/(Norm[i].dot(r.direction));	//plane intersection
		if((t >= tmin)&&(t < t0))		
		{
			Vec3f P = r.origin + t*r.direction;			//point of intersection
			switch(i)									//checking box boundrarys
			{
				case 0:
				case 1:
					if(P.y >= Min.y && P.y <= Max.y)
						if(P.z >= Min.z && P.z <= Max.z)
						{
							t0 = t;
							Side = i;
						}
					break;
				case 2:
				case 3:
					if(P.x >= Min.x && P.x <= Max.x)
						if(P.z >= Min.z && P.z <= Max.z)
						{
							t0 = t;
							Side = i;
						}
					break;
				case 4:
				case 5:
					if(P.y >= Min.y && P.y <= Max.y)
						if(P.x >= Min.x && P.x <= Max.x)
						{
							t0 = t;
							Side = i;
						}
					break;
			}
		}
	}
	if(Side != -1)
	{
		h.t = t0;
		h.material = material_;
		h.normal = Norm[Side];
		return true;
	}
	return false;
}

bool Plane::intersect( const Ray& r, Hit& h, float tmin ) const {
	// YOUR CODE HERE (R5)
	// Intersect the ray with the plane.
	// Pay attention to respecting tmin and h.t!
	// Equation for a plane:
	// ax + by + cz = d;
	// normal . p - d = 0
	// (plug in ray)
	// origin + direction * t = p(t)
	// origin . normal + t * direction . normal = d;
	// t = (d - origin . normal) / (direction . normal);
	
	//P0 = (x0,y0,z0)
	//n = (A,B,C)
	//n dot P + D = 0
	//D = -Ax0 - Bx0 - Cx0
	
	Vec3f P = (normal_ * offset_);					//point on plane
	float D = -normal_.dot(P); 
	//H(P) = n .P  + D = 0
	//P(t) = r.origin + t * r.direction
	//H(P) = n . (r.origin + t * r.direction) + D = 0
	//H(P) = n . r.origin + t*n . r.direction + D = 0
	//H(P) = t = - D - n . r.origin / n . r.direction

	float t  = (-D -normal_.dot(r.origin))/(normal_.dot(r.direction));
	if((t < h.t) && (t >= tmin))
	{
		h.t = t;
		h.material = material_;
		h.normal = normal_;
		return true;
	}
	return false;
}

Transform::Transform(const Mat4f& m, Object3D* o) :
	matrix_(m),
	object_(o)
{
	assert(o != nullptr);
	inverse_ = matrix_.inverted();
	inverse_transpose_ = inverse_.transposed();
}

bool Transform::intersect(const Ray& r, Hit& h, float tmin) const {
	// YOUR CODE HERE (EXTRA)
	// Transform the ray to the coordinate system of the object inside,
	// intersect, then transform the normal back. If you don't renormalize
	// the ray direction, you can just keep the t value and do not need to
	// recompute it!
	// Remember how points, directions, and normals are transformed differently!

	Vec4f T_Origin = inverse_*Vec4f(r.origin, 1);
	Vec4f T_Dir = inverse_*Vec4f(r.direction, 0);

	Vec3f TO = Vec3f(T_Origin.x, T_Origin.y, T_Origin.z);
	Vec3f TD = Vec3f(T_Dir.x, T_Dir.y, T_Dir.z);

	Ray T_Ray(TO, TD);
	bool Intersect = object_->intersect(T_Ray, h, tmin);
	if(Intersect)
	{
		Vec4f T_Norm = Vec4f(h.normal,0);
		Vec4f Norm = inverse_transpose_*T_Norm;
		h.normal = Vec3f(Norm.x,Norm.y,Norm.z);
		h.normal.normalize();
		return true;
	}
	else
		return false; 
}

bool Sphere::intersect( const Ray& r, Hit& h, float tmin ) const {
	// Note that the sphere is not necessarily centered at the origin.
	
	Vec3f tmp = center_ - r.origin;
	Vec3f dir = r.direction;

	float A = dot(dir, dir);
	float B = - 2 * dot(dir, tmp);
	float C = dot(tmp, tmp) - sqr(radius_);
	float radical = B*B - 4*A*C;
	if (radical < 0)
		return false;

	radical = sqrtf(radical);
	float t_m = ( -B - radical ) / ( 2 * A );
	float t_p = ( -B + radical ) / ( 2 * A );
	Vec3f pt_m = r.pointAtParameter( t_m );
	Vec3f pt_p = r.pointAtParameter( t_p );

	assert(r.direction.length() > 0.9f);

	bool flag = t_m <= t_p;
	if (!flag) {
		::printf( "sphere ts: %f %f %f\n", tmin, t_m, t_p );
		return false;
	}
	assert( t_m <= t_p );

	// choose the closest hit in front of tmin
	float t = (t_m < tmin) ? t_p : t_m;

	if (h.t > t  && t > tmin) {
		Vec3f normal = r.pointAtParameter(t);
		normal -= center_;
		normal.normalize();
		h.set(t, this->material(), normal);
		return true;
	}
	return false;
} 

Triangle::Triangle(const Vec3f& a, const Vec3f& b, const Vec3f& c,
	Material *m, const Vec2f& ta, const Vec2f& tb, const Vec2f& tc, bool load_mesh) :
	Object3D(m)
{
	vertices_[0] = a;
	vertices_[1] = b;
	vertices_[2] = c;
	texcoords_[0] = ta;
	texcoords_[1] = tb;
	texcoords_[2] = tc;

	if (load_mesh) {
		preview_mesh.reset((FW::Mesh<FW::VertexPNT>*)FW::importMesh("preview_assets/tri.obj"));
		set_preview_materials();
	}
}

bool Triangle::intersect( const Ray& r, Hit& h, float tmin ) const {
	// YOUR CODE HERE (R6)
	// Intersect the triangle with the ray!
	// Again, pay attention to respecting tmin and h.t!

	//P(t) = P(beta,gamma)
	//R0 +t*Rd = a + beta(b-a) + gamma(c-a)
	//beta + gamma < 1 & beta > 0 & gamma > 0
	Mat3f A;
	Vec3f a = vertices_[0];
	Vec3f b = vertices_[1];
	Vec3f c = vertices_[2];

	A.setCol(0, a-b);
	A.setCol(1, a-c);
	A.setCol(2, r.direction);

	Vec3f B(a - r.origin);
	Vec3f X = A.inverted()*B;			//brute force
	float t = X.z;
	if((X.x > 0 && X.y > 0) && X.x+X.y < 1)	//check beta and gamma boundrarys
		if((t < h.t) && (t >= tmin))
		{
			h.t = t;
			h.material = material_;
			h.normal = (b-a).cross(c-b);	
			h.normal.normalize();
			return true;
		}
	return false;
}

const Vec3f& Triangle::vertex(int i) const {
	assert(i >= 0 && i < 3);
	return vertices_[i];
}


