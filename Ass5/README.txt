﻿# ME-C3100 Computer Graphics, Fall 2015
# Lehtinen / Kemppinen, Ollikainen, Puomio
#
# Assignment 5: Ray Tracing

Student name:Viktor Måsala
Student number:84732N
Hours spent on requirements (approx.):20
Hours spent on extra credit (approx.):20

# First, a 10-second poll about this assignment period:

Did you go to exercise sessions?
yes
Did you work on the assignment using Aalto computers, your own computers, or both?
both
# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

R1 Generating rays & ambient lighting (  1p): done
R2 Visualizing depth		      (0.5p): done
R3 Perspective camera		      (0.5p): done
R4 Phong shading		      (  3p): done
R5 Plane intersection		      (0.5p): done
R6 Triangle intersection	      (  1p): done
R7 Shadows			      (  1p): done
R8 Mirror reflection		      (  1p): done
R9 Antialiasing			      (1.5p): attempted

# Did you do any extra credit work?
-Refraction
-Transform and box: had to do alot of  wierd stuff to get the preview to look right from how I understood the input(ca 2h trial and error), box can be used like the other objects included box.txt as example
-Transparent shadows, does not work if light is inside transparent object. also shown in box.txt

(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

# Are there any known problems/bugs remaining in your code?
-R4 Phong shading: not quite working with low exponent.. edges glow dont know why, guessing i am not clamping it correctly or a rounding error somehow
-R9 Antialiasing: dont really understand it at all. i'm super confused on what i am supposed to do. as is it only accepts 4,9,16 samples since the others are not square(untouched code). It kinda works thou
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?
I found this excersise lacking in comments, many times i looked in code only to find variables that i wasn't sure what they were since i only had a name to go by, and functions that were unclear what they do/are supposed to do.
example class Box has min_ and max_ and no explanation what they are or how to use them.
Otherwise i found this round to be the most interesting this far.
(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

