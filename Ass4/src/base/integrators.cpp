#include "integrators.hpp"

#include "utility.hpp"
#include "particle_systems.hpp"

void eulerStep(ParticleSystem& ps, float step) {
	// YOUR CODE HERE (R1)
	// Implement an Euler integrator.
	auto X_0 = ps.state();
	auto F_0 = ps.evalF(X_0);
	int n = X_0.size();
	auto X_1 = State(n);
	for(int i = 0;i < n;++i)
	{
		X_1[i] = X_0[i] + step*F_0[i];
	}
	ps.set_state(X_1);

};

void trapezoidStep(ParticleSystem& ps, float step) {
	// YOUR CODE HERE (R3)
	// Implement a trapezoid integrator.
	auto X_0 = ps.state();
	auto F_0 = ps.evalF(X_0);
	int n = X_0.size();
	auto X_1 = State(n);
	auto X_2 = State(n);
	for(int i = 0; i < n; ++i)
	{
		X_1[i] = X_0[i] + step*F_0[i];
	}
	auto F_1 = ps.evalF(X_1);

	for(int i = 0; i < n; ++i)
	{
		X_2[i] = X_0[i] + step*0.5f*(F_0[i]+F_1[i]);
	}
	ps.set_state(X_2);
}

void midpointStep(ParticleSystem& ps, float step) {
	const auto& x0 = ps.state();
	auto n = x0.size();
	auto f0 = ps.evalF(x0);
	auto xm = State(n), x1 = State(n);
	for (auto i = 0u; i < n; ++i) {
		xm[i] = x0[i] + (0.5f * step) * f0[i];
	}
	auto fm = ps.evalF(xm);
	for (auto i = 0u; i < n; ++i) {
		x1[i] = x0[i] + step * fm[i];
	}
	ps.set_state(x1);
}

void rk4Step(ParticleSystem& ps, float step)
{
	// EXTRA: Implement the RK4 Runge-Kutta integrator.
	auto X1 = ps.state();
	int n = X1.size();
	
	auto X2 = State(n);
	auto X3 = State(n);
	auto X4 = State(n);

	auto K1 = State(n);
	auto K2 = State(n);
	auto K3 = State(n);
	auto K4 = State(n);

	auto RK4 = State(n);
	
	K1 = ps.evalF(X1);

	for(int i = 0; i < n; ++i)
	{
		X2[i] = X1[i] + step*0.5f*K1[i];
	}
	K2 = ps.evalF(X2);

	for(int i = 0; i < n; ++i)
	{
		X3[i] = X1[i] + step*0.5f*K2[i];
	}
	K3 = ps.evalF(X3);

	for(int i = 0; i < n; ++i)
	{
		X4[i] = X1[i] + step*K3[i];
	}
	K4 = ps.evalF(X4);

	for(int i = 0; i < n; ++i)
	{
		RK4[i] = X1[i] + (K1[i] + 2.0f*K2[i] + 2.0f*K3[i] + K4[i])*step/6.0f;
	}
	ps.set_state(RK4);
}
 
