#include "particle_systems.hpp"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>

#include "utility.hpp"

using namespace std;
using namespace FW;

namespace {

inline Vec3f fGravity(float mass) {
	return Vec3f(0, -9.8f * mass, 0);
}

// force acting on particle at pos1 due to spring attached to pos2 at the other end
inline Vec3f fSpring(const Vec3f& pos1, const Vec3f& pos2, float k, float rest_length)
{
	// YOUR CODE HERE (R2)
	// Note that you can visualize the forces using the draw_lines function found in utility.hpp. This might make debugging easier for you.
	Vec3f tmp;
	Vec3f X = (pos1 - pos2);
	float len = X.length();
	tmp = -k*(len - rest_length)*(X/len);

	return tmp;
}

inline Vec3f fDrag(const Vec3f& v, float k)
{
	// YOUR CODE HERE (R2)
	Vec3f tmp = -k*v;
	return tmp;
}

} // namespace

void SimpleSystem::reset()
{
	state_ = State(1, Vec3f(0, radius_, 0));
}

State SimpleSystem::evalF(const State& state) const {
	State f(1, Vec3f(-state[0].y, state[0].x, 0));
	return f;
}

Points SimpleSystem::getPoints() {
	return Points(1, state_[0]);
}

Lines SimpleSystem::getLines() {
	static const auto n_lines = 50u;
	auto l = Lines(n_lines * 2);
	const auto angle_incr = 2*FW_PI/n_lines;
	for (auto i = 0u; i < n_lines; ++i) {
		l[2*i] = l[2*i+1] =
			Vec3f(radius_ * FW::sin(angle_incr * i), radius_ * FW::cos(angle_incr * i), 0);
	}
	rotate(l.begin(), l.begin()+1, l.end());
	return l;
}

void SpringSystem::reset()
{
	const auto start_pos = Vec3f(0.1f, -0.5f, 0.0f);
	const auto spring_k = 30.0f;
	state_ = State(4);
	// YOUR CODE HERE (R2)
	// Set the initial state for a particle system with one particle fixed
	// at origin and another particle hanging off the first one with a spring.
	// Place the second particle initially at start_pos.
	state_[0] = 0;				//position	x
	state_[1] = 0;				//velocity	v
	state_[2] = start_pos;
	state_[3] = 0;
	const auto RestLength = start_pos.length();
	spring_ = Spring(0, 1, spring_k, RestLength);
}

State SpringSystem::evalF(const State& state) const {
	const auto drag_k = 0.5f;
	const auto mass = 1.0f;

	State f(4);
	// YOUR CODE HERE (R2)
	// Return a derivative for the system as if it was in state "state".
	// You can use the fGravity, fDrag and fSpring helper functions for the forces.
	f[0] = 0;					//velocity		v
	f[1] = 0;					//acceleration	F/m
	f[2] = state[3];
	f[3] = fGravity(mass)+fSpring(state[2], state[0], spring_.k, spring_.rlen)+fDrag(state[3], drag_k) + Vec3f(0, 0, Wind);
	return f;
}

Points SpringSystem::getPoints() {
	auto p = Points(2);
	p[0] = state_[0]; p[1] = state_[2];
	return p;
}

Lines SpringSystem::getLines() {
	auto l = Lines(2);
	l[0] = state_[0]; l[1] = state_[2];
	return l;
}

void PendulumSystem::reset()
{
	const auto spring_k = 1000.0f;
	const auto start_point = Vec3f(0);
	const auto end_point = Vec3f(0.05, -1.5, 0);
	state_ = State(2*n_);
	// YOUR CODE HERE (R4)
	// Set the initial state for a pendulum system with n_ particles
	// connected with springs into a chain from start_point to end_point.
	Spring S;
	const auto Step = end_point/(n_-1);		//dividing the chain into segments
	S.rlen = Step.length(); //same for all of them
	S.k = spring_k;
	for(int i = 0; i < n_; ++i)
	{
		state_[i*2] = Step*i;		//position	x
		state_[i*2+1] = 0;			//velocity	v
		if(i+1 < n_)
		{
			S.i1 = i;
			S.i2 = i+1;
			springs_.push_back(S);
		}
	}

}


State PendulumSystem::evalF(const State& state) const {
	const auto drag_k = 0.5f;
	const auto mass = 0.5f;
	auto f = State(2*n_);
	// YOUR CODE HERE (R4)
	// As in R2, return a derivative of the system state "state".
	for(int i = 1; i < n_; ++i)
	{
		f[i*2] = state[i*2+1];		//velocity	v
		
		f[i*2+1] = (fGravity(mass)+fSpring(state[i*2], state[i*2-2], springs_.at(i-1).k, springs_.at(i-1).rlen)+fDrag(state[i*2+1], drag_k))/mass + Vec3f(0, 0, Wind);		//acceleration	F/m
		if(i+1 < n_)				//second spring
			f[i*2+1] +=	fSpring(state[i*2], state[i*2+2], springs_.at(i).k, springs_.at(i).rlen)/mass;
	}

	return f;
}

Points PendulumSystem::getPoints() {
	auto p = Points(n_);
	for (auto i = 0u; i < n_; ++i) {
		p[i] = state_[i*2];
	}
	return p;
}

Lines PendulumSystem::getLines() {
	auto l = Lines();
	for (const auto& s : springs_) {
		l.push_back(state_[2*s.i1]);
		l.push_back(state_[2*s.i2]);
	}
	return l;
}

void ClothSystem::reset() {
	const auto spring_k = 300.0f;
	const auto width = 1.5f, height = 1.5f; // width and height of the whole grid
	state_ = State(2*x_*y_);
	// YOUR CODE HERE (R5)
	// Construct a particle system with a x_ * y_ grid of particles,
	// connected with a variety of springs as described in the handout:
	// structural springs, shear springs and flex springs.

	Objects = std::vector<Sphere>(1);		//adding a sphere
	
	Spring Sx, Sy, Ss, Sfx, Sfy;		//all types of springs
	//auto StepX = Vec3f(LBorder, 0, 0)/(x_-1);
	//auto StepY = Vec3f(LBorder, 0, 0)/(y_-1);
	auto StepX = width/(x_-1);
	auto StepY = height/(y_-1);
	auto Start = Vec3f(-width/2, 0, 0);				//top left corner

	Sx.rlen = StepX;								// structural springs
	Sx.k = spring_k;
	Sx.i1 = 0;
	Sx.i2 = 0;
	Sy.rlen = StepY;
	Sy.k = spring_k;
	Sy.i1 = 0;
	Sy.i2 = 0;

	Ss.rlen = std::sqrt(StepX*StepX + StepY*StepY);	// shear springs
	Ss.k = spring_k;
	Ss.i1 = 0;
	Ss.i2 = 0;

	Sfx.rlen = StepX*2;								// flex springs
	Sfx.k = spring_k;
	Sfy.rlen = StepY*2;
	Sfy.k = spring_k;
	Sfx.i1 = 0;
	Sfx.i2 = 0;
	Sfy.i1 = 0;
	Sfy.i2 = 0;

	springs_.push_back(Sx);			//UGLY HACK: adding the values(k and lenghts) of the springs as the first five springs of the vector
	springs_.push_back(Sy);
	springs_.push_back(Ss);
	springs_.push_back(Sfx);
	springs_.push_back(Sfy);

	for(int iy = 0; iy < y_; ++iy)
	{
		for(int ix = 0; ix < x_; ++ix)
		{
			state_[iy*x_*2 + ix*2] =Start + Vec3f(StepX * ix, 0, -StepY * iy);		//position
			state_[iy*x_*2 + ix*2 +1] = 0;											//velocity
			
			if(ix + 1 < x_)							// structural springs
			{
				Sx.i1 = iy*x_ + ix;
				Sx.i2 = iy*x_ + ix +1;
				springs_.push_back(Sx);
			}
			if(iy + 1 < y_)
			{
				Sy.i1 = iy*x_ + ix;
				Sy.i2 = (iy+1)*x_ + ix;
				springs_.push_back(Sy);
			}
			
			if((ix + 1 < x_)&&(iy + 1 < y_))		// shear springs
			{
				Ss.i1 = iy*x_ + ix;
				Ss.i2 = (iy+1)*x_ + ix +1;
				springs_.push_back(Ss);
			}
			if((ix - 1 >= 0)&&(iy + 1 < y_))
			{
				Ss.i1 = iy*x_ + ix;
				Ss.i2 = (iy+1)*x_ + ix-1;
				springs_.push_back(Ss);
			}
			if(ix + 2 < x_)							// flex springs
			{
				Sfx.i1 = iy*x_ + ix;
				Sfx.i2 = iy*x_ + ix +2;
				springs_.push_back(Sfx);
			}
			if(iy + 2 < y_)
			{
				Sfy.i1 = iy*x_ + ix;
				Sfy.i2 = (iy+2)*x_ + ix;
				springs_.push_back(Sfy);
			}
		}
	}
}

State ClothSystem::evalF(const State& state) const {
	const auto drag_k = 0.08f;
	const auto n = x_ * y_;
	static const auto mass = 0.025f;
	auto f = State(2*n);
	// YOUR CODE HERE (R5)
	// This will be much like in R2 and R4.
	float Sk = springs_[0].k; //same for all
	float Sxl = springs_[0].rlen;	//lengths for different types of springs
	float Syl = springs_[1].rlen;
	float Ssl = springs_[2].rlen;
	float Sfxl = springs_[3].rlen;
	float Sfyl = springs_[4].rlen;

	for(int iy = 0; iy < y_; ++iy)
	{
		for(int ix = 0; ix < x_; ++ix)
		{
			Vec3f F = 0;
			Vec3f V = state[iy*x_*2 + ix*2 +1];
			

			if(ix+1 < x_)																			//x+
			{
				F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+1)*2], Sk, Sxl);			//struct
				if(iy+1 < y_)
					F += fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix+1)*2], Sk, Ssl);	//shear down
				if(iy-1 >= 0)
					F += fSpring(state[iy*x_*2 + ix*2], state[(iy-1)*x_*2 + (ix+1)*2], Sk, Ssl);	//shear up
				if(ix+2 < x_)
					F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+2)*2], Sk, Sfxl);		//flex

			}
			if(ix-1 >= 0)																			//x-
			{
				F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix-1)*2], Sk, Sxl);			//struct
				if(iy+1 < y_)
					F += fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix-1)*2], Sk, Ssl);	//shear down
				if(iy-1 >= 0)
					F += fSpring(state[iy*x_*2 + ix*2], state[(iy-1)*x_*2 + (ix-1)*2], Sk, Ssl);	//shear up
				if(ix-2 >= 0)
					F += fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix-2)*2], Sk, Sfxl);		//flex

			}
			if(iy+1 < y_)																			//y+
			{
				F += fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + ix*2], Sk, Syl);			//struct
				if(iy+2 < y_)
					F += fSpring(state[iy*x_*2 + ix*2], state[(iy+2)*x_*2 + ix*2], Sk, Sfyl);		//flex
			}
			if(iy-1 >= 0)																			//y-
			{
				F += fSpring(state[iy*x_*2 + ix*2], state[(iy-1)*x_*2 + ix*2], Sk, Syl);			//struct
				if(iy-2 >= 0)
					F += fSpring(state[iy*x_*2 + ix*2], state[(iy-2)*x_*2 + ix*2], Sk, Sfyl);		//flex
			}
			f[iy*x_*2 + ix*2 +1] = (F + fGravity(mass)+fDrag(V, drag_k))/mass + Vec3f(0, 0, Wind);	//force/mass
			
			if(Collisions)
			for(Sphere Object : Objects)
			{
				Vec3f P1 = state[iy*x_*2 + ix*2];
				Vec3f P2 = Object.Origin;
				Vec3f D = P1-P2;
				if(D.length() < Object.Radius)			//collision check
				{
					//float tmp = std::abs(V.dot(D/D.length()));		//simple collision abs makes it always point outwards from the sphere
					//V += 2.0f*D/D.length()*(tmp);
					V = D/D.length();									//move the cloth outward
					//f[iy*x_*2 + ix*2 +1] += fDrag(V, drag_k)/mass;
				}
			}

			
			f[iy*x_*2 + ix*2] = V;				//velocity

			//Vec3f Tmp = 0;																		//tried only calculating spring forces only once per spring but it was slower than the above
			//if(ix+1 < x_)																			//x+
			//{
			//	Tmp = fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+1)*2], Sk, Sxl)/mass;		//struct
			//	f[iy*x_*2 + ix*2 +1] += Tmp;														//force on this point
			//	f[iy*x_*2 + (ix+1)*2 +1] -= Tmp;													//force on other point
			//	if(iy+1 < y_)
			//	{
			//		Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix+1)*2], Sk, Ssl)/mass;	//shear down
			//		f[iy*x_*2 + ix*2 +1] += Tmp;
			//		f[(iy+1)*x_*2 + (ix+1)*2 +1] -= Tmp;
			//	}
			//	if(ix+2 < x_)
			//	{
			//		Tmp = fSpring(state[iy*x_*2 + ix*2], state[iy*x_*2 + (ix+2)*2], Sk, Sfxl)/mass;	//flex
			//		f[iy*x_*2 + ix*2 +1] += Tmp;
			//		f[iy*x_*2 + (ix+2)*2 +1] -= Tmp;
			//	}
			//	
			//}
			//if(ix-1 >= 0)																			//x-
			//{
			//	if(iy+1 < y_)
			//	{
			//		Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + (ix-1)*2], Sk, Ssl)/mass;	//shear down
			//		f[iy*x_*2 + ix*2 +1] += Tmp;
			//		f[(iy+1)*x_*2 + (ix-1)*2 +1] -= Tmp;
			//	}
			//}
			//if(iy+1 < y_)																			//y+
			//{
			//	Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+1)*x_*2 + ix*2], Sk, Syl)/mass;			//struct
			//	f[iy*x_*2 + ix*2 +1] += Tmp;
			//	f[(iy+1)*x_*2 + ix*2 +1] -= Tmp;
			//	if(iy+2 < y_)
			//	{
			//		Tmp = fSpring(state[iy*x_*2 + ix*2], state[(iy+2)*x_*2 + ix*2], Sk, Sfyl)/mass;		//flex
			//		f[iy*x_*2 + ix*2 +1] += Tmp;
			//		f[(iy+2)*x_*2 + ix*2 +1] -= Tmp;
			//	}
			//}
			//f[iy*x_*2 + ix*2 +1] += (fGravity(mass)+fDrag(V, drag_k))/mass;					//force/mass
		}
	}
	f[0] = 0;		//corners fixed
	f[0+1] = 0;

	f[(x_-1)*2] = 0;
	f[(x_-1)*2 +1] = 0;
	return f;
}

Points ClothSystem::getPoints() {
	auto n = x_ * y_;
	auto p = Points(n);
	for (auto i = 0u; i < n; ++i) {
		p[i] = state_[2*i];
	}
	return p;
}

Lines ClothSystem::getLines() {
	auto l = Lines();
	for (const auto& s : springs_) {
		l.push_back(state_[2*s.i1]);
		l.push_back(state_[2*s.i2]);
	}
	return l;
}

void SprinklerSystem::reset()
{
	state_ = State(2*N);
	PList = std::vector<Particle>(N);
	Objects = std::vector<Sphere>(2);
	Objects[1].Origin = Vec3f(0,0,0.5);
	Objects[1].Radius = 0.3;

	for(int i = 0; i < N; ++i)
	{
		state_[i*2] = Vec3f(0,0,0);														//position	x
		state_[i*2+1] = Vec3f(-1,1,-1) + 0.25f*Vec3f(rand()%10, rand()%10, rand()%10);	//velocity	v
		PList[i].TTL = 5+rand()%TTL;													//time to live
	}
}

State SprinklerSystem::evalF(const State& state) const 
{
	auto f = State(2*N);
	float mass = 1;
	for(int i = 0; i < N; ++i)
	{
		Vec3f V = state[i*2+1];

		//f[i*2+1] = fGravity(mass) + fDrag(state[i*2+1], 0.5f);		//acceleration	F/m
		f[i*2+1] = fGravity(mass) + Vec3f(0, 0, Wind);					//acceleration	F/m

		if(Collisions)
		for(Sphere Object : Objects)
		{
			Vec3f P1 = state[i*2];
			Vec3f P2 = Object.Origin;
			Vec3f D = P1-P2;
			if(D.length() < Object.Radius)
			{
				float tmp = std::abs(V.dot(D/D.length()));		//simple collision abs makes it always point outwards from the sphere
				V += 2.0f*D/D.length()*(tmp);
			}
		}
		f[i*2] = V;
	}
	return f;
}

Points SprinklerSystem::getPoints()
{
	auto n = N;
	auto p = Points(n);
	for(auto i = 0u; i < n; ++i)
	{
		p[i] = state_[2*i];

		PList[i].TTL -= 0.01;							//check life
		if(PList[i].TTL < 0)
		{												//reset particle
			state_[i*2] = Vec3f(0, 0, 0);														//position	x
			state_[i*2+1] = Vec3f(-1, 1, -1) + 0.25f*Vec3f(rand()%10, rand()%10, rand()%10);	//velocity	v
			PList[i].TTL = 5+rand()%TTL;														//time to live
		}
	}
	return p;
}