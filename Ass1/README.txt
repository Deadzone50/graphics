﻿# ME-C3100 Computer Graphics, Fall 2015
# Lehtinen / Kemppinen, Ollikainen
#
# Assignment 1: Introduction

Student name: Viktor Måsala
Student number: 84732N
Hours spent on requirements (approx.):4h
Hours spent on extra credit (approx.):4h

# First, some questions about where you come from and how you got started.
# Your answers in this section will be used to improve the course.
# They will not be judged or affect your points, but please answer all of them.
# Keep it short; answering shouldn't take you more than 5-10 minutes.

- What are you studying here at Aalto? (Department, major, minor...?)
Major: electronics
Minor: computer science

- Which year of your studies is this?
5th

- Is ME-C3100 a mandatory course for you?
no

- Have you had something to do with graphics before? Other studies, personal interests, work?
not really, have tried some simple stuff on my own, was going to try to write a raytracer so this course happens to fit into my intrests quite well

- Do you remember basic linear algebra? Matrix and vector multiplication, cross product, that sort of thing?
yes, some

- How is your overall programming experience? What language are you most comfortable with?
i program in my spare time as a hobby using mostly c++

- Do you have some experience with these things? (If not, do you have experience with something similar such as C or Direct3D?)
C++: yes
C++11: yes
OpenGL: i have read some tutorials while doing some stuff on my own

- Have you used a version control system such as Git, Mercurial or Subversion? Which ones?
i have used subversion for the c++ programming course at aalto

- Did you go to the technology lecture?
yes

- Did you go to exercise sessions?
yes some

- Did you work on the assignment using Aalto computers, your own computers, or both?
both

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

(Try to get everything done! Based on previous data, virtually everyone who put in the work and did well in the first two assignments ended up finishing the course, and also reported a high level of satisfaction at the end of the course.)

                            opened this file (0p): done
                         R1 Moving an object (1p): done
R2 Generating a simple cone mesh and normals (3p): done
  R3 Converting mesh data for OpenGL viewing (3p): done
           R4 Loading a large mesh from file (3p): done

# Did you do any extra credit work?
(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)
-Version Controll
-Rotate And Scale, rotate with arrow keys, scale x with insert/delete
-Animation, press r to activate. The timer class was really confusing and i couldnt get it to work as i wanted it, starting and stopping the timer with r.
-Viewport correction
-Viewport and perspective, replaces the viewport correction  u/j sets the FOV, not sure if this is working as it should
-Better Camera, controlled with mouse, the rotataion is wierd since it translates before it roatates to keep the camera axes correct. guess i have to somehow transform the translation matrix to do it in the correct order

# Are there any known problems/bugs remaining in your code?

(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

# Did you collaborate with anyone in the class?
no

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?

(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)

